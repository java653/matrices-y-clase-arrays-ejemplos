import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        String[] matrizTexto = new String[3]; //Creo una matriz unidimensional de 3 elementos (tipo String).

        //Defino los valores de cada uno de los índices la matriz.
        matrizTexto[0] = "Hola, ";
        matrizTexto[1] = "me llamo Sergio. ";
        matrizTexto[2] = "Tengo 28 años y soy de Cádiz. ";

        //Creo un bucle for para recorrer cada uno de los índices de la matriz e imprimirlos en pantalla.
        for(int i = 0; i < matrizTexto.length; i++){
            System.out.println(matrizTexto[i]);
        }

        //Declaro esta vez una matriz de números enteros y meto los valores directamente (no uno a uno como antes).
        int[] matrizNumeros = {5,17,45,23,12,56,89,100};

        System.out.println(Arrays.stream(matrizNumeros).average());//Saco la media de la matrizNumeros y la imprimo.

        Arrays.sort(matrizNumeros);//Ordeno la matrizNumeros.

        for(int i = 0; i < matrizNumeros.length; i++){ //Bucle for para imprimir la matrizNumeros ordenada.
            System.out.println(matrizNumeros[i]);
        }

        int[] matrizNumeros2 = {2,56,43,234,56,45,32,234}; //Declaro otra matriz de números enteros nueva.

        int[] matrizCopiaDeMatrizNumeros = Arrays.copyOf(matrizNumeros,0); //Declaro una nueva matriz y copio los índices de la matrizNumeros en la nueva.

        for(int i = 0; i < matrizCopiaDeMatrizNumeros.length; i++){ //Bucle for para imprimir la matriz copia y comprobar que la copia se hizo correctamente.
            System.out.println(matrizCopiaDeMatrizNumeros[i]);
        }

        //Declaro dos matrices nuevas de números enteros.
        int[] matrizEjemplo = {50,25,78,90};
        int[] matrizEjemplo2 = new int[3];

        //Copio desde el índice 0 hasta el índice 2 (3 elementos) de matrizEjemplo y los almaceno en matrizEjemplo2.
        matrizEjemplo2 = Arrays.copyOfRange(matrizEjemplo,0,3);

        for(int numero : matrizEjemplo2){ //Bucle foreach para recorrer la matrizEjemplo2 e imprimirla.
            System.out.println(numero);
        }

        System.out.println(Arrays.stream(matrizEjemplo2).max()); //Saco el máximo de los valores de la matrizEjemplo2.
        System.out.println(Arrays.stream(matrizEjemplo2).min()); //Saco el mínimo de los valores de la matrizEjemplo2.

        int[] matrizEjemplo3 = {30,25,90,23}; //Declaro una nueva matriz numérica.

        Arrays.sort(matrizEjemplo3); //Ordeno la matrizEjemplo3.

        for(int numero : matrizEjemplo3){ //Recorro la matrizEjemplo3 con un foreach para imprimir sus valores.
            System.out.println(numero);
        }

        System.out.println(Arrays.binarySearch(matrizEjemplo3, 90)); //Para hacer binarySearch, primero hay que ordenar la matriz (lo que hice antes). Este binarySearch busca el valor dentro de la matriz que tú le pongas y te dice en qué índice de la matriz está.

        int[] matrizEjemplo4 = {2,35,67,89,5};//Creo una matriz nueva.
        Arrays.fill(matrizEjemplo4,24);//Con Arrays.fill relleno toda la matriz con un 24.
        for(int numero : matrizEjemplo4){//Imprimo los valores de la matriz con un foreach.
            System.out.println(numero);
        }

        int[] matrizEjemplo5 = {10,20,30,40,50};//Creo otra matriz.
        Arrays.fill(matrizEjemplo5,4,5,25);//Esta vez no solo relleno con un valor, le digo que el valor del índice 4 me lo sustituya por un 25 y que llegue hasta el elemento 5.
        for(int numero : matrizEjemplo5){//Recorro la matriz con un foreach e imprimo sus valores.
            System.out.println(numero);
        }

        int[] matrizEjemplo6 = {50,60,70,80,90};//Creo otra matriz.
        Arrays.fill(matrizEjemplo6,1,4,1);//Relleno esa matriz desde el índice 1 hasta el elemento 4 y la relleno de unos.
        for(int numero : matrizEjemplo6){//Recorro la matriz con un foreach para imprimir sus valores.
            System.out.println(numero);
        }


        //PARA COMPARAR ARRAYS.
        int[] a = {10,20};
        int[] b = {10,20};
        int[] c = {25,50};
        int[] d = {25,50};
        int[] e = {25,50,10,23};
        int[] f = {23,45,45,23,42};
        System.out.println(a.equals(b)); //Compara la localización en memoria de ambos objetos. Al ver que no son iguales, pone false.
        System.out.println(Arrays.equals(a,b));//Arrays.equals dice que a y b es true porque compara los valores de ambos, al ser iguales, obviamente pone que es verdadero.
        System.out.println(Arrays.compare(a,b));//En este caso da 0 porque el primer array (a) es igual al segundo array (b).
        System.out.println(Arrays.compare(a,c));//En este caso de -1 porque el segundo array (c) es mayor que el primero (a).
        System.out.println(Arrays.compare(d,a));//En este caso da 1 porque el primer array (d) es mayor que el segundo (a).
        System.out.println(Arrays.mismatch(a,b));//Da -1 porque ambos arrays son iguales.
        System.out.println(Arrays.mismatch(d,e));//El índice donde los arrays difieren por primera vez. Por eso da un 2.
        System.out.println(Arrays.mismatch(a,f));//Da un 0 cuando los arrays son completamente distintos.


        //Imprimir matriz con Arrays.toString.
        System.out.println(Arrays.toString(f));

        //Comentarios para commit.
    }
}